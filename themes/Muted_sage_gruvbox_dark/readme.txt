1) Install global theme Muted_sage.._plasma
2) Install cursor to /usr/share/icons
	Default cursor everywhere:
		vim ~/.icons/default/index.theme

		[Icon Theme]
		Inherits=Oxygen_Zion

		then:
		ln -s /usr/share/icons/Oxygen_Zion/cursors ~/.icons/default/cursors
3) Install gtk application style Muted_sage.._gtk in ~/.themes/
	Install gtk icons Delight_green to ~/.local/share/icons and link to /usr/share/icons/
	Install LXAppearance in order to define the separate GTK icons
		Edit ~/.gtkrc-2.0(mine) to modify stuff
4) Color schemes
plasma -> ~/.local/share/color/schemes
kwrite ->  "/kwrite
konsole -> "/konsole
tilix -> ~/.config/tilix/schemes/

5) VIM
    1. Copy `/path/to/gruvbox-material/autoload/gruvbox_material.vim` to
    `~/.vim/autoload/` .

    2. Copy `/path/to/gruvbox-material/colors/gruvbox-material.vim` to
    `~/.vim/colors/` .

    3. Copy `/path/to/gruvbox-material/doc/gruvbox-material.txt` to
    `~/.vim/doc/` and execute `:helptags ~/.vim/doc/` to generate help tags.

6) EMACS
Define custom theme directory by doinc: M-x: customize-variable: custom-theme-directory
Copy the doom-gruvbox-material-theme.el in =~/.emacs.d/themes/. 
Add (load-theme 'doom-gruvbox-material t)
Change hightlight or something ? M-x customize-face ! Dont know how it's called ? M-x describe-face !

7) Firefox + Thunderbird:
Self explanatory
