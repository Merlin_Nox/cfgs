(define-package "helm" "20230501.1445" "Helm is an Emacs incremental and narrowing framework"
  '((helm-core "3.9.0")
    (popup "0.5.3"))
  :commit "3c2c20a17dd343bfa20567cf0b764e56777f9fab" :authors
  '(("Thierry Volpiatto" . "thievol@posteo.net"))
  :maintainers
  '(("Thierry Volpiatto" . "thievol@posteo.net"))
  :maintainer
  '("Thierry Volpiatto" . "thievol@posteo.net")
  :url "https://emacs-helm.github.io/helm/")
;; Local Variables:
;; no-byte-compile: t
;; End:
