;;; Generated package description from popup-complete.el  -*- no-byte-compile: t -*-
(define-package "popup-complete" "20141109.308" "completion with popup" '((popup "0.5.0")) :commit "e362d4a005b36646ffbaa6be604e9e31bc406ca9" :authors '(("Syohei YOSHIDA" . "syohex@gmail.com")) :maintainer '("Syohei YOSHIDA" . "syohex@gmail.com") :url "https://github.com/syohex/emacs-popup-complete")
