(define-package "gruvbox-theme" "20230503.744" "A retro-groove colour theme for Emacs"
  '((autothemer "0.2"))
  :commit "c06ef3766be6681d995e88564f6bc9e95f9fde9a" :authors
  '(("Jason Milkins" . "jasonm23@gmail.com"))
  :maintainers
  '(("Jason Milkins" . "jasonm23@gmail.com"))
  :maintainer
  '("Jason Milkins" . "jasonm23@gmail.com")
  :url "http://github.com/greduan/emacs-theme-gruvbox")
;; Local Variables:
;; no-byte-compile: t
;; End:
