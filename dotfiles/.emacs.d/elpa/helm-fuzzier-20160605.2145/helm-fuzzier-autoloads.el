;;; helm-fuzzier-autoloads.el --- automatically extracted autoloads  -*- lexical-binding: t -*-
;;
;;; Code:

(add-to-list 'load-path (directory-file-name
                         (or (file-name-directory #$) (car load-path))))


;;;### (autoloads nil "helm-fuzzier" "helm-fuzzier.el" (0 0 0 0))
;;; Generated autoloads from helm-fuzzier.el

(defvar helm-fuzzier-mode nil "\
Non-nil if Helm-Fuzzier mode is enabled.
See the `helm-fuzzier-mode' command
for a description of this minor mode.
Setting this variable directly does not take effect;
either customize it (see the info node `Easy Customization')
or call the function `helm-fuzzier-mode'.")

(custom-autoload 'helm-fuzzier-mode "helm-fuzzier" nil)

(autoload 'helm-fuzzier-mode "helm-fuzzier" "\
helm-fuzzier minor mode

This is a minor mode.  If called interactively, toggle the
`Helm-Fuzzier mode' mode.  If the prefix argument is positive,
enable the mode, and if it is zero or negative, disable the mode.

If called from Lisp, toggle the mode if ARG is `toggle'.  Enable
the mode if ARG is nil, omitted, or is a positive number.
Disable the mode if ARG is a negative number.

To check whether the minor mode is enabled in the current buffer,
evaluate `(default-value \\='helm-fuzzier-mode)'.

The mode's hook is called both when the mode is enabled and when
it is disabled.

\(fn &optional ARG)" t nil)

(register-definition-prefixes "helm-fuzzier" '("helm-fuzzier-"))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; helm-fuzzier-autoloads.el ends here
