;;; typst-mode-autoloads.el --- automatically extracted autoloads
;;
;;; Code:


;;;### (autoloads nil "typst-mode" "typst-mode.el" (0 0 0 0))
;;; Generated autoloads from typst-mode.el

(add-to-list 'auto-mode-alist '("\\.typ\\'" . typst-mode))

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "typst-mode" '("typst-")))

;;;***

(provide 'typst-mode-autoloads)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; typst-mode-autoloads.el ends here
