###Merlins bash configuration file###
#Sexy Prompt#
 PS1='\[\e[0;38;5;113m\]\w \[\e[0;91m\]>\[\e[0;38;5;202m\]>\[\e[0;38;5;34m\]> \[\e[0m\]' 
#set vim as editor
set -o vi

###RC###
alias vim="vim.gtk3"
alias bashrc="vim ~/.bashrc"
alias fishrc="vim ~/.fishrc"
alias fishc="vim ~/.config/fish/config.fish"
alias vimrc="vim ~/.vimrc"
alias emacsrc="vim ~/.emacs"

alias dumpix="dconf dump /com/gexperts/Tilix/ > ~/.cfgs/rc_tt/tilix.dconf && dconf dump /com/gexperts/Tilix/ > ~/cookies/rc/rc_tt/tilix.dconf"
alias loadix="dconf load /com/gexperts/Tilix/ < ~/rc/rc_tt/tilix.dconf"

alias bzsh="cp -v  ~/{.bashrc,.fishrc} ~/.cfgs/dotfiles/shell && cp -v  ~/{.bashrc,.fishrc} ~/cookies/rc/shell && echo 'bash and zsh rc git updated !'"
alias zbsh="cp -v  ~/rc/shell/{.bashrc,.fishrc} ~/ && echo 'local bash and zsh rc updated !'"


###GLOBAL###

##Halp and grep###
alias halp="cat ~/.cfgs/halp.txt | grep -i"
alias vhalp="vim ~/.cfgs/halp.txt"
alias bg="cat ~/.bashrc | grep -i"
alias vreadme="vim ~/.cfgs/readme.txt"
alias creadme="cat ~/.cfgs/readme.txt"

##Edit system files##
alias fstab="sudo vim /etc/fstab"

##QOL##
export LC_ALL=en_US.UTF-8 #Set this pesky locale
alias ulsblk="lsblk -o name,mountpoint,size,label,fstype,uuid"
alias la="ls --human-readable --group-directories-first --size -1 -S --classify -lah"
alias lls="ls --human-readable --group-directories-first --size -1 -S --classify -l"
alias cp="cp -rv"
alias mv="cp -rv"
alias rm="rm -v"
alias rrf="rm -rvf"

##UTILITY##
alias NOGG="rm -v *.ogg"
alias NAAC="rm -v *.aac"
alias pingg="ping google.com"
alias perf="sudo cpupower frequency-set -g performance"
alias demand="sudo cpupower frequency-set -g ondemand"
alias cpuinfo="cpupower frequency-info"
alias ipa="ip a | grep 192"

##DEB SPECIFIC##
alias sag="sudo grub-update"
alias sai="sudo apt install -y"
alias sar="sudo apt remove -y"
alias saa="sudo apt autoremove -y"
alias sap="sudo apt purge"
alias sau="sudo apt update && sudo apt upgrade -y"
alias sas="sudo apt search"
alias sal="sudo apt list"
alias saes="sudo apt edit-sources"
alias saesmx="sudo vim /etc/apt/sources.list.d/mx.list"
alias saar="sudo add-apt-repository"
alias saarr="sudo add-apt-repository --remove"

###GIT###
alias g="git"
alias ga="git add"
alias gaa="git add --all"
alias gb="git branch"
alias gba="git branch -a"
alias gbd="git branch -d"
alias gbD="git branch -D"
alias gcm="git checkout master"
alias gcmsg="git commit -m"
alias ggsup="git branch --set-upstream-to=origin/'(git_current_branch)'"
alias gpu="git push upstream"
alias gpv="git push -v"
alias gpsup="git push --set-upstream origin '(git_current_branch)'"
alias ghh="git help"
alias gm="git merge"
alias gf="git fetch"
alias gfa="git fetch --all --prune"
alias gfo="git fetch origin"
alias gp="git push"
alias gl="git pull"


###SYS_COMMANDS###
alias sp="sudo shutdown -h now"
alias sr="sudo reboot"
alias kre="kquitapp5 plasmashell || killall plasmashell && kstart5 plasmashell"

###MISC###
export PATH="$HOME/.local/bin:$PATH"

##STEEL##
alias light="rivalcfg --z1 101010 --z2 101010 --z3 101010 -c black &&  sudo apexctl colors 8 8 8 8 1"
alias dark="rivalcfg --z1 black --z2 black --z3 black -c black && sudo apexctl colors 1 1 1 1 1"
alias disco="rivalcfg -e disco"
alias rainbow="rivalcfg -e rainbow-shift"
alias rainbowb="rivalcfg -e rainbow-breath"
alias breath="rivalcfg -e breath"
alias breathfast="rivalcfg -e breath-fast"

###APP SHORTCUTS###
alias sc="lutris lutris:rungame/starcraft-ii&"
alias lol="cd /home/nox/.Games/LoL/ && ./launch-script.py&"
alias rsave="cp -vr /home/nox/.config/retroarch/saves/* /home/nox/cookies/saves/retroarch"
alias andro_audio="adb shell push --sync /mnt/EXT001/Audio /storage/emulated/0/"
alias mo2="protontricks -c 'wine /home/nox/.steam/steam/steamapps/compatdata/3842230751/pfx/drive_c/Modding/MO2/ModOrganizer.exe' 3842230751"
alias jd="/home/nox/.jd2/JDownloader2&"
alias hand="HandBrakeCLI -i -o  -v -Z \"H.265 MKV 720p30\" --all-subtitles --all-audio"
alias cputemp="watch -n 1 sensors"
alias fisho="echo alias \" \" >> ~/.fishrc" #lazy fish alias append...
alias beeper="bash /home/nox/cookies/scripts/minouteur.sh"

###Recently added aliases###
