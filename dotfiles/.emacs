;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; File name: ` ~/.emacs '
;;; ---------------------
;;;
;;; If you need your own personal ~/.emacs
;;; please make a copy of this file
;;; an placein your changes and/or extension.
;;;
;;; Copyright (c) 1997-2002 SuSE Gmbh Nuernberg, Germany.
;;;
;;; Author: Werner Fink, <feedback@suse.de> 1997,98,99,2002
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Test of Emacs derivates
;;; -----------------------
;(if (string-match "XEmacs\\|Lucid" emacs-version)
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;;; XEmacs
  ;;; ------
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; (progn
;    (if (file-readable-p "~/.xemacs/init.el")
;       (load "~/.xemacs/init.el" nil t))
; )
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;;; GNU-Emacs
  ;;; ---------
  ;;; load ~/.gnu-emacs or, if not exists /etc/skel/.gnu-emacs
  ;;; For a description and the settings see /etc/skel/.gnu-emacs
  ;;;   ... for your private ~/.gnu-emacs your are on your one.
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; (if (file-readable-p "~/.gnu-emacs")
;     (load "~/.gnu-emacs" nil t)
;   (if (file-readable-p "/etc/skel/.gnu-emacs")
;(load "/etc/skel/.gnu-emacs" nil t)))

  ;; Custom Settings
  ;; ===============
  ;; To avoid any trouble with the customization system of GNU emacs
  ;; we set the default file ~/.gnu-emacs-custom
  (setq custom-file "~/.gnu-emacs-custom")
  (load "~/.gnu-emacs-custom" t t)
(require 'subr-x)
;;;
;))
;;;
;; load package manager, add the Melpa package registry
(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(package-initialize)

;; bootstrap use-package
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))
(require 'use-package)

(use-package evil
  :ensure t
  :defer .1 ;; don't block emacs when starting, load evil immediately after startup
  :init
  (setq evil-want-integration t) ;; required by evil-collection
  (setq evil-want-keybinding nil) ;; required by evil-collection
  (setq evil-search-module 'evil-search)
  (setq evil-ex-complete-emacs-commands nil)
  (setq evil-shift-round nil)
  :config
  (evil-mode)

  ;; vim-like keybindings everywhere in emacs
  (use-package evil-collection
    :after evil
    :ensure t
    :config
    (evil-collection-init))

;;Load fancy-dabbrev.el:
;(require 'fancy-dabbrev)
;;; Enable fancy-dabbrev previews everywhere:
;(global-fancy-dabbrev-mode)
;;; If you want TAB to indent the line like it usually does when the cursor
;;; is not next to an expandable word, use 'fancy-dabbrev-expand-or-indent
;;; instead of `fancy-dabbrev-expand`:
;(global-set-key (kbd "TAB") 'fancy-dabbrev-expand-or-indent)
;global-set-key (kbd "<backtab>") 'fancy-dabbrev-backward)

; GEPETO CODE BELOW LEGACY ABOVE ; 
;; Load fancy-dabbrev.el only if the package is installed:
(when-let (fancy-dabbrev-package (require 'fancy-dabbrev nil t))
  ;; Enable fancy-dabbrev previews everywhere:
  (global-fancy-dabbrev-mode)
  ;; If you want TAB to indent the line like it usually does when the cursor
  ;; is not next to an expandable word, use 'fancy-dabbrev-expand-or-indent
  ;; instead of `fancy-dabbrev-expand`:
  (global-set-key (kbd "TAB") 'fancy-dabbrev-expand-or-indent)
  (global-set-key (kbd "<backtab>") 'fancy-dabbrev-backward))

  ;; gl and gL operators, like vim-lion
  (use-package evil-lion
    :ensure t
    :bind (:map evil-normal-state-map
                ("g l " . evil-lion-left)
               ("g L " . evil-lion-right)
                :map evil-visual-state-map
                ("g l " . evil-lion-left)
                ("g L " . evil-lion-right)))

  ; like vim-surround
  (use-package evil-surround
    :ensure t
    :commands
    (evil-Surround-region)
    :init
    (evil-define-key 'visual global-map "gS" 'evil-Surround-region))

  (message "Welcome abord matey !"))

;;other plugins to load
(setq frame-title-format "Emacs")
(set-language-environment "UTF-8")
(menu-bar-mode -1)
(tool-bar-mode -1)
(scroll-bar-mode -1)
(global-linum-mode t)
(setq ring-bell-function 'ignore)

(load-theme 'gruvbox-dark-medium t)

(require 'web-mode) ; ; ; web mode in the following exts
(add-to-list 'auto-mode-alist '("\\.html\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.php\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.css\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.js\\'" . web-mode))

(use-package emmet-mode)
(add-hook 'web-mode-hook 'emmet-mode)

(require 'yasnippet) ; ; ; dont forget to customize the web mode folder !
(yas-global-mode 1)

(global-auto-complete-mode t) ; ; ; loads major mode dicts
(add-to-list 'ac-dictionary-directories "~/.emacs.d/ac-dict")

(projectile-global-mode)
(setq projectile-completion-system 'helm)
(helm-projectile-on)

(use-package helm
	:bind (
	("M-x" . helm-M-x)
	("s-e" . helm-etags-select)
	("s-g" . helm-find-files)
	("s-f" . helm-for-files)
	([f10] . helm-buffers-list)))

;;; load etags asyncronously
  (let* ((async-shell-command-buffer 'new-buffer)
         (display-buffer-alist '(("Async Shell Command" display-buffer-no-window))))
;;;      (async-shell-command "ctags -e -R"))
  )

;;;In order to get support for many of the LaTeX packages you will use in your documents, you should enable document parsing as well, which can be achieved by putting
 	
(setq TeX-auto-save t)
(setq TeX-parse-self t)

(use-package plantuml-mode
     :ensure t
      :init
	(setq org-plantuml-jar-path "/home/nox/.emacs.d/plantuml.jar")
	(setq plantuml-jar-path (expand-file-name "/home/nox/.emacs.d/plantuml.jar")	)
      :config
      (add-to-list 'auto-mode-alist '("\\.uml\\'" . org-mode))
      (add-to-list 'auto-mode-alist '("\\.plantuml\\'" . org-mode))
  )
;; active Org-babel languages
(org-babel-do-load-languages
 'org-babel-load-languages
 '(;; other Babel languages
   (plantuml . t)))


;;;Configure recentf
(recentf-mode 1)
(setq recentf-max-menu-items 25)
(setq recentf-max-saved-items 25)

;;;Save recentf files !
(run-at-time nil (* 5 60) 'recentf-save-list)

;;; ace window
(global-set-key (kbd"C-<tab>") 'ace-window)
;;; split window
(global-set-key (kbd"s-j") 'split-window-right)
(global-set-key (kbd"s-k") 'split-window-below)

;;; enable desktop save mode
(desktop-save-mode 1)
;;; desktop saves marker
(add-to-list 'desktop-locals-to-save 'evil-markers-alist)
; ; ; straight 
(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 6))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/radian-software/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))
; ; ; typst
(use-package typst-mode
  :straight (:type git :host github :repo "Ziqi-Yang/typst-mode.el"))
