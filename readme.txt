###FUCKING BELL##
>system config > kde workspace

###Files###
Copy the holy cookie Also .emacs.d, .vimrc, .emacs, .bashrc .ssh .fishrc .wine
config: fish smplayer lutris tilix retroarch
.local/share: lutris vulkan konsole
cache: wine
~/.config/lutris/
~/.cache/lutris/
~/.local/share/lutris/

###KDE###
KDE notes are here /.local/share/plasma_notes/

#To remove all title bars in KDE plasma 5, create a Window Rule:

    Go to System Settings > Window Management > Window Rules > New..
    Under Window class (application) choose Regular Expression from the drop down and enter .* in the input.
    Click the Appearance & Fixes tab. Enable the No titlebar and frame option. Select Force from the drop down click the Yes radio option.

Save and close everything, and all your title bars will disappear! This is how your settings should look:

#Window behaviour > follow the mouse

#workspace behaviour > single click
#Disable screen locking...
#Quiet mode disabled in grub
/etc/default/grub

###VIM###
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
vim +PluginInstall +qall
:w !sudo tee %

###Switching to fish?###
If you wish to use fish (or any other shell) as your default shell, you need to enter your new shell's executable in two places.

Add the shell to /etc/shells with:

> echo /usr/bin/fish | sudo tee -a /etc/shells

Change your default shell with:

> chsh -s /usr/bin/fish

###Themes etc###
##Locations
#Color schemes: .local/share/color-schemes
#Cursors & icons: /usr/share/icons and or .local/share/icons
#Tilix: ~/.config/tilix/schemes/
#Konsole: ~/.local/share/konsole/
#Vim:Sainhe Emacs: customize face for typst
#gtk Application style: ~/.themes/
###GTK3 theme different icons from QT's >>> lxappearance
Make installed icons available system wide:
~ ❯❯❯ sudo ln -s .local/share/icons/Delight\ -\ Green/ /usr/share/icons/
~ ❯❯❯ ls /usr/share/icons/

####CURSOR CHANGES####
vim ~/.icons/default/index.theme
[Icon Theme]
Inherits=Oxygen_Zion
then:
ln -s /usr/share/icons/Oxygen_Zion/cursors ~/.icons/default/cursors

###git###
git config --global user.email "merlin09100@gmail.com"
git config --global user.name "Nox"
chmod 400 ~/.ssh/id_rsa
chmod 600 ~/.ssh/id_rsa

####GAMES####
PS2
%command% --disable-gpu
SC2: install and copy gamedata & users
Lol: reinstall...

####GRUB THEME####

###FAUDIO
WINETRICKS=/home/nox/.steam/steam/steamapps/compatdata/2723037518/ bash ~faudio-20.07/wine_setu.sh
OR just install it under winecfg xaudio...

###Screens being borky
Identify screens with xrandr --listmonitors
edit /etc/x11/xorg.conf
Section "Monitor"

Identifier "DisplayPort-1"

Option "Primary" "true"

EndSection


Section "Monitor"

Identifier "HDMI-A-0"

Option "above" "DisplayPort-1"
Option "pos" "345x0"

EndSection

###MISC
autostart [I] ~ ❯❯❯ sudo cp /usr/share/applications/syncthing-start.desktop .config/autostart/
8bitdo: /etc/modprobe.d/blacklist_hid_nintendo.conf or whatever name for the .conf file:

blacklist hid_nintendo
