#!/bin/sh
for i in /sys/bus/usb/devices/*/power/autosuspend;
do echo 1 > $i;
done

for j in /sys/bus/usb/devices/*/power/level;
do echo auto > $j;
done

for k in /sys/bus/usb/devices/usb*/power/control;
do echo auto > $k;
done

for l in /sys/bus/usb/devices/*/power/wakeup;
do echo disabled > $l;
done
