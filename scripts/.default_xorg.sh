#!/bin/sh
xrandr --output DisplayPort-0 --off --output DisplayPort-1 --primary --mode 2560x1440 --rate 144.00 --pos 0x1080 --rotate normal --output DisplayPort-2 --off --output HDMI-A-0 --mode 1920x1080 --rotate normal
