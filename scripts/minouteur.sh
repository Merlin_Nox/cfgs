#!/bin/bash

# Prompt the user to enter the timer duration
echo "Set timer (number + unit as in 12m or 2s)"
read input

# Extract the unit from the input (s for seconds or m for minutes)
unit="${input: -1}"

# Remove the unit from the input so we can convert it to seconds
dateU="${input::-1}"

# Convert the input to seconds
if [ "$unit" == "s" ]; then
  dateU=$((dateU))
else
  dateU=$((dateU * 60))
fi

# Calculate the start and end times for the timer
dateSta=$(date +%s)
dateEnd=$((dateSta + dateU))

# Display the start time and the end time
echo "An alarm has been set!"
echo "start: $(date -d @$dateSta)"
echo "end: $(date -d @$dateEnd)"

# Calculate the number of seconds in the interval
intervallS=$((dateEnd - dateSta))

# Display the remaining time every second until the timer reaches the end time
for ((i=1; i<=$intervallS; i++)) do
  sleep 1s

  # Calculate the remaining time in seconds
  remaining=$((dateEnd - $(date +%s)))

  # Convert the remaining time to minutes and seconds
  remaining=$(date -ud @$remaining +%H:%M:%S)

  # Display the remaining time
  printf "\rremaining: $remaining"
done

# Print a message when the timer reaches the end time
echo "Alarm end: $(date -d @$dateEnd)"
echo "Press any key to exit"

while [ true ] ;
	do read -t 0.5 -n 1
		if [ $? = 0 ] ;
		then exit ;

		else
		spd-say meow
		fi

# Wait for the user to press a key to exit the script
done

