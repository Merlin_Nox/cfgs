for i in /sys/bus/usb/devices/*/power/autosuspend;
do echo -1 > $i;
done

for j in /sys/bus/usb/devices/*/power/level;
do echo on > $j;
done

for k in /sys/bus/usb/devices/*/power/control;
do echo on > $k;
done

for l in /sys/bus/usb/devices/*/power/wakeup;
do echo enabled > $l;
done
